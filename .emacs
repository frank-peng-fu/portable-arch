;; Frank Fu's emacs to go file


(require 'package)
(setq package-enable-at-startup nil)
(add-to-list 'package-archives '("melpa" . "http://melpa.org/packages/"))
(add-to-list 'package-archives '("gnu" . "http://elpa.gnu.org/packages/"))
(package-initialize)

;; install use-package if it is not installed
(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))

(eval-when-compile
  (require 'use-package))

;; set font and size
;; (setq default-frame-alist '((font . "Monaco 20")))

;; put custom-set-variables into a separate file under .emacs.d.
(setq custom-file (concat user-emacs-directory "custom.el"))
(load custom-file 'noerror)


;; gui and other niceties.
(global-auto-revert-mode 1)

(blink-cursor-mode 0)

(window-divider-mode)

(global-hl-line-mode 1)

(setq ring-bell-function 'ignore)

(if (fboundp 'scroll-bar-mode) (scroll-bar-mode -1))

(if (fboundp 'menu-bar-mode) (menu-bar-mode -1))

(if (fboundp 'tool-bar-mode) (tool-bar-mode -1))

(require 'linum)

(setq column-number-mode t)

(show-paren-mode 1)

(require 'recentf)

(recentf-mode 1)

(setq recentf-max-menu-items 50)

;; transparency
;; (set-frame-parameter (selected-frame) 'alpha '(95 . 95))
;; (add-to-list 'default-frame-alist '(alpha . (95 . 95)))

;; global keybindings
(global-set-key "\C-x\C-m" 'execute-extended-command)

(global-set-key "\C-x\C-k" 'kill-region)

(global-set-key "\C-w" 'backward-kill-word)

(global-set-key "\C-c\C-k" 'kill-region)

(global-set-key "\M-5" 'query-replace)

;; LaTeX setup, including synctex
;; LaTeX setup, including synctex
(use-package tex
  :ensure auctex
  :config
  (setq TeX-auto-save t)
  (setq TeX-parse-self t)
  (setq-default TeX-master nil)
  (add-hook 'LaTeX-mode-hook 'flyspell-mode)
  (add-hook 'plain-TeX-mode-hook
	  (lambda () (set (make-local-variable 'TeX-electric-math)
			  (cons "$" "$"))))
  (add-hook 'LaTeX-mode-hook
	  (lambda () (set (make-local-variable 'TeX-electric-math)
			  (cons "$" "$"))))
  (setq LaTeX-electric-left-right-brace t)
  (add-hook 'LaTeX-mode-hook 'turn-on-reftex)
  (add-hook 'LaTeX-mode-hook 'TeX-source-correlate-mode)
  (setq reftex-plug-into-AUCTeX t) 	
  (setq TeX-electric-sub-and-superscript t)
  (setq TeX-view-program-selection '((output-pdf "Zathura")))
  ;; for mac
  ;; (setq TeX-view-program-list
  ;;       '(("Preview.app" "open -a Preview.app %o")
  ;;         ("Skim" "open -a Skim.app %o")
  ;;         ("displayline" "displayline -g -b %n %o %b")
  ;;         ("open" "open %o"))
  ;;       TeX-view-program-selection
  ;;       '((output-dvi "open")
  ;;         (output-pdf "Skim")
  ;;         (output-html "open")))
  )

(use-package tex
  :ensure auctex
  :config
  (setq TeX-auto-save t)
  (setq TeX-parse-self t)
  (setq-default TeX-master nil)
  (add-hook 'LaTeX-mode-hook 'flyspell-mode)
  (add-hook 'plain-TeX-mode-hook
	  (lambda () (set (make-local-variable 'TeX-electric-math)
			  (cons "$" "$"))))
  (add-hook 'LaTeX-mode-hook
	  (lambda () (set (make-local-variable 'TeX-electric-math)
			  (cons "$" "$"))))
  (setq LaTeX-electric-left-right-brace t)
  (add-hook 'LaTeX-mode-hook 'turn-on-reftex)
  (add-hook 'LaTeX-mode-hook 'TeX-source-correlate-mode)
  (setq reftex-plug-into-AUCTeX t) 	
  (setq TeX-electric-sub-and-superscript t)

  )



;; start emacs server 
(server-start) 



;; magit
(use-package magit
  :ensure t
  :bind
  (("C-x g" . magit-status))
  )


;; set theme
(use-package doom-themes
  :ensure t
  ;; :config
  ;; (load-theme 'doom-one-light t)
  )

(use-package theme-changer
  :ensure t
  :config
  (setq calendar-latitude 44.6488)
  (setq calendar-longitude -63.582687)
  (change-theme 'doom-solarized-light 'doom-one)
  )


;; Helm mode, swiper and projectile.
(use-package swiper-helm
  :ensure t
  :bind
  (("M-x" . helm-M-x)
   ("C-x C-f" . helm-find-files)
   ("C-x b" . helm-mini)
   ("C-S" . swiper-helm)
   )
  :config
  (helm-mode 1)
  (setq helm-buffers-fuzzy-matching t
      helm-recentf-fuzzy-match    t
      helm-mode-fuzzy-match t
      )
  )

;; projectile
(use-package projectile
  :ensure t
  :bind
  (("C-c p" . projectile-command-map))
  :config
  (projectile-mode +1)
  )

;; projectile
(use-package helm-projectile
  :ensure t
  :config
  (helm-projectile-on)
  )

(use-package company
  :ensure t
  :config
  (add-hook 'after-init-hook 'global-company-mode)
  )

(use-package company-math
  :ensure t
  )

(use-package company-reftex
  :ensure t
  )

(use-package company-bibtex
  :ensure t
  )

(use-package company-dict
  :ensure t
  )

(use-package company-auctex
  :ensure t
  )


;; org mode set up
(require 'org)
(define-key global-map "\C-cl" 'org-store-link)
(define-key global-map "\C-ca" 'org-agenda)
;; (setq org-directory "~/org/")
;; (setq org-agenda-files (list "~/org/schedule.org"
;;                              ))
(add-hook 'org-mode-hook 'turn-on-flyspell)
(setq org-todo-keywords '((sequence "TODO(t)" "PROJ(p)" "IDEA(i)" "STRT(s)" "HOLD(h)" "|" "DONE(d)" "CANCELLED(k)")))
(add-hook 'text-mode-hook 'turn-on-flyspell)

;; use org-bullets
(use-package org-bullets
  :ensure t
  :init
  (add-hook 'org-mode-hook (lambda ()
                             (org-bullets-mode 1))))


;; (use-package haskell-mode
;;   :ensure t
;;   :config
;;   (add-hook 'haskell-mode-hook 'turn-on-haskell-indentation)
;;   (add-hook 'haskell-mode-hook 'interactive-haskell-mode)
;;   )



